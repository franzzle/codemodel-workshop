package com.cgi.workshops.codemodel;

import com.sun.codemodel.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Generates code
 */
public class CodeGenerator {
    
    public static void main(String[] args) throws IOException {
        JCodeModel codeModel = new JCodeModel();

        try {
            JDefinedClass generatedBookClass = codeModel._class("com.cgi.workshop.Book");



            JMethod constructor = generatedBookClass.constructor(JMod.PUBLIC);
            JFieldVar field1 = generatedBookClass.field(JMod.PRIVATE + JMod.FINAL, String.class, "isbn");
            JFieldVar field2 = generatedBookClass.field(JMod.PRIVATE + JMod.FINAL, String.class, "titel");
            JVar param1 = constructor.param(String.class, "isbn");
            JVar param2 = constructor.param(String.class, "titel");
            JBlock body = constructor.body();
            body.assign(JExpr._this().ref(field1), param1);
            body.assign(JExpr._this().ref(field2), param2);



        } catch (JClassAlreadyExistsException e) {
            System.out.println(String.format("De klasse %s bestaat al", e.getExistingClass().name()));
        }

        System.out.println(CodeGenerator.class.getPackage().getName());

        File destDir = new File(System.getProperty("user.home"), "ws_codemodel/generated");

        if(!destDir.exists()){
            destDir.mkdirs();
        }
        codeModel.build(destDir);

    }
}
