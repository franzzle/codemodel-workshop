JCodeModel              : De verzameling classes (vaak je model) die je wilt genereren 
JDefinedClass           : Definieert alles wat een class behelst, is een kind van JCodeModel
JMethod                 : Definieert alles wat een method behelst, is een kind van JDefinedClass
JBlock                  : De inhoud van een constructor, method, static initializer etc.. Overal waar een body met inhoudelijke code moet komen.
JVar                    : Te gebruiken voor Constructor parameters en lokale variabelen
JFieldVar               : Een specifieke variabele, namelijk de member variabelen
JDocComment             : Javadoc die op een field, methode
JCommentPart            : Een onderdeel van de Javadoc, @param parameters bijv.
JRef                    : Referentie naar een klasse
JAnnotationUse          : Een annotatie die je op een veld, klasse of methode kan plakken
JAnnotationArrayMember  : Een array van annotaties, vaak gebruikt om configuratie (als strings) mee te geven. 
JMod                    : De modifier (denk aan public,private,final etc..) van methodes, klasses en fields
JType                   : Met dit onderdeel kan je het type van je veld beter specificeren, bijv List<String> of een void kunnen niet als Class doorgegeven worden.
JInvocation             : Iets wat uit te voeren is. Je kan hier argumenten aan meegeven. 
