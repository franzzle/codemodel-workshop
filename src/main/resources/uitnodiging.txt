Over mijzelf


Generate Code with Codemodel

Met Codemodel kan je op een hele snelle manier java code genereren.
Codemodel is een spinoff van Jaxb. 
Misschien gebruik je dit wel elke dag zonder dat je het weet!
Waarom zou jij niet die 'power' van deze library gebruiken?
Misschien heb je een probleem in je project wat uitstekend met Codemodel opgelost kan worden!
Denk bijv aan model classes genereren op basis van json, dbms,csv of een menu structuur in xhtml?
In deze workshop wordt het gebruik van deze krachtige library gedemonstreerd.
Daarna gaan we zelf aan de hand van recepten classes genereren...zodat we nooit meer hoeven te tikken.

Tools & Technieken

Any IDE
Any Maven
